'use strict'
const express = require('express');
const cors = require('cors');

const Api = require('./constants/api')
const mainRouter = require('./controllers');
const app = express();

app.use(cors());
app.use(express.json());



app.use(Api.ROOT_URL,mainRouter)


app.all('*',function(req,res){
  res.status(404).send("Not Found")
}); 


app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

process.on('unhandledRejection', error => {
  console.log('unhandledRejection');
});

module.exports = app;
