module.exports = {
	ROOT_URL: '/api/v/1',
	AUTH: '/auth',
	DASHBOARD: '/dashboard',
	PROFILE: '/profile'
};
