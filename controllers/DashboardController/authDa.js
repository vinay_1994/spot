'use strict';
const DbCon = require('../../utils/dbConnection');
const ownFileJson = require('./../../constants/ownFile.json');
const sharedFIleJson = require('./../../constants/sharedFIleJson.json');
const Service = require('./dashboardservice');

const SERVICE = {
	saveFiletoDb: async (filedata, userdata) => {
		let userName = await DbCon.query('select user_name from tbl_users_master where user_id = ?', userdata);
		if (userName) filedata['user_name'] = userName[0]['user_name'];
		filedata['user_id'] = userdata;
		let fileLocation = filedata.location.split('.');
		let getJsonExists = await DbCon.query('select * from tbl_file_details where user_id = ?', userdata);
		if (getJsonExists.length !== 0) {
			let ownJson = JSON.parse(getJsonExists[0]['own_files']);
			if (fileLocation[fileLocation.length - 1] === 'mp4') {
				ownJson.video.push(await Service.uploadOwnFileJson(filedata));
				await DbCon.query('update tbl_file_details set own_files = ? where user_id = ?', [
					JSON.stringify(ownJson),
					userdata
				]);
			} else {
				ownJson.files.push(await Service.uploadOwnFileJson(filedata));
				await DbCon.query('update tbl_file_details set own_files = ? where user_id = ?', [
					JSON.stringify(ownJson),
					userdata
				]);
				JSON.parse(getJsonExists[0]['own_files']).files.push(await Service.uploadOwnFileJson(filedata));
			}
		} else {
			if (fileLocation[fileLocation.length - 1] === 'mp4') {
				ownFileJson.video.push(await Service.uploadOwnFileJson(filedata));
			} else {
				ownFileJson.files.push(await Service.uploadOwnFileJson(filedata));
			}
			let uploadData = {
				user_id: userdata,
				own_files: JSON.stringify(ownFileJson),
				shared_files: JSON.stringify(sharedFIleJson),
				user_name: filedata['user_name']
			};
			await DbCon.query('insert into tbl_file_details set ?', uploadData);
		}
	},
	getUserContent: async userId => {
		try {
			return await DbCon.query('select * from tbl_file_details where user_id = ?', userId);
		} catch (error) {
			throw error.sqlMessage;
		}
	},
	shareFiles: async shareFileDetails => {
		try {
			let isShareUser = await DbCon.query(
				'select * from tbl_users_master where contact_num = ?',
				shareFileDetails['contact_num']
			);
			if (isShareUser.length === 0) {
				throw 'Sharing User Does Not Exist';
			} else {
				shareFileDetails.file.fileUploadType = 'Shared';
				let getSharedJson = await DbCon.query(
					'select * from tbl_file_details where user_id = ?',
					isShareUser[0]['user_id']
				);
				if (getSharedJson.length !== 0) {
					let sharedJson = JSON.parse(getSharedJson[0]['shared_files']);
					if (shareFileDetails.file.type === 'video/mp4') {
						sharedJson.video.push(shareFileDetails.file);
						await DbCon.query('update tbl_file_details set shared_files = ? where user_id = ?', [
							JSON.stringify(sharedJson),
							isShareUser[0]['user_id']
						]);
					} else {
						sharedJson.files.push(shareFileDetails.file);
						await DbCon.query('update tbl_file_details set shared_files = ? where user_id = ?', [
							JSON.stringify(sharedJson),
							isShareUser[0]['user_id']
						]);
					}
				} else {
					let arr = sharedFIleJson;
					if (shareFileDetails.file.type === 'video/mp4') {
						arr.video.push(shareFileDetails.file);
					} else {
						arr.files.push(shareFileDetails.file);
					}
					let fileShareData = {
						user_id: isShareUser[0]['user_id'],
						own_files: JSON.stringify(ownFileJson),
						shared_files: JSON.stringify(arr),
						user_name: isShareUser[0]['user_name']
					};
					await DbCon.query('insert into tbl_file_details set ?', fileShareData);
				}
			}
			return true;
		} catch (error) {
			throw error;
		}
	}
};

module.exports = SERVICE;
