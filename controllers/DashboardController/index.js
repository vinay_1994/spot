'use strict';
const route = require('express').Router();
const httpUtil = require('../../utils/HttpUtil');
const s3Upload = require('../../utils/s3filesManager/fileupload');
const DbService = require('./authDa');
const Service = require('./dashboardservice');

route.get('/', async (req, res) => {
	try {
		//validation goes hear
		let data = await DbService.getUserContent(req.user['user_id']);
		if (data.length === 0) {
			res.json(httpUtil.getSuccess('No Data', []));
		} else {
			res.json(httpUtil.getSuccess('All results caugth up', await Service.filterResult(data)));
		}
	} catch (error) {
		res.json(httpUtil.getException('Something Went Wrong', error));
	}
});

route.post('/upload', async (req, res) => {
	try {
		s3Upload
			.uploadToS3(req, res)
			.then(async resolved => {
				await DbService.saveFiletoDb(resolved.file, req.user['user_id']);
				res.json(httpUtil.getSuccess('File Uploaded Successfully', [resolved.file.key]));
			})
			.catch(rej => {
				res.json(httpUtil.getException('Error in uploading the file', rej.toString()));
			});
	} catch (error) {
		res.json(httpUtil.getException('Something Went Wrong', error));
	}
});

/*
@param contact_num
@param file{}
*/
route.post('/share', async (req, res) => {
	try {
		await DbService.shareFiles(req.body);
		res.json(httpUtil.getSuccess('Shared Success fully', []));
	} catch (error) {
		res.json(httpUtil.getException(error, []));
	}
});

module.exports = route;
