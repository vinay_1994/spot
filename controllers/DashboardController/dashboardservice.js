'use strict';

exports.filterResult = async data => {
	let file = [],
		video = [];
	let response = {
		own_files: {
			video: [],
			files: []
		},
		shared_files: {
			video: [],
			files: []
		}
	};
	JSON.parse(data[0]['own_files']).video.forEach(item => {
		response['own_files'].video.push(item);
	});
	JSON.parse(data[0]['own_files']).files.forEach(item => {
		response['own_files'].files.push(item);
	});
	JSON.parse(data[0]['shared_files']).video.forEach(item => {
		response['shared_files'].video.push(item);
	});
	JSON.parse(data[0]['shared_files']).files.forEach(item => {
		response['shared_files'].files.push(item);
	});
	return response;
};

exports.uploadOwnFileJson = fileInfo => {
	let currentDate = new Date();
	return {
		file_name: fileInfo.key,
		type: fileInfo.mimetype,
		author: fileInfo['user_name'],
		size: Math.round(fileInfo.size / 1000),
		created_date: currentDate.toDateString(),
		created_time: currentDate.toTimeString(),
		owner_id: fileInfo['user_id'],
		file_id: `${fileInfo['user_id']}_${Date.now()}`,
		filePath: fileInfo.location.split('//')[1],
		fileUploadType: 'own'
	};
};
