'use strict';
const route = require('express').Router();
const httpUtil = require('../../utils/HttpUtil');
const MSG = require('./messages').MSG;
const SmsSender = require('../../utils/smsSender');
const JwtToken = require('./../../utils/JwtUtil');
const Validation = require('./validation');
const modalDa = require('./authDa');
const generateOtp = require('../../utils/otpgenerator').generateOtp;

route.post('/register', async (req, res) => {
	try {
		let userData = req.body;
		await Validation.registrationValidatioin(userData);
		let user = await modalDa.userExistingCheck(userData);
		if (user.length > 0) {
			res.json(httpUtil.getBadRequest(MSG.userExists));
		} else {
			userData['otp'] = await generateOtp();
			await modalDa.saveUser(userData);
			//await SmsSender(userData)
			let userId = await modalDa.getUserId(userData);
			if (userId.length === 1) {
				res.json(
					httpUtil.getSuccess(MSG.MailSuccess, { otp: userId[0]['otp'], user_id: userId[0]['user_id'] })
				);
			} else {
				res.json(httpUtil.getBadRequest(MSG.somethingWentWrong, null));
			}
		}
	} catch (error) {
		res.json(httpUtil.getBadRequest(error.toString(), null));
	}
});

route.post('/verification/:userId', async (req, res) => {
	try {
		await modalDa.Verify(req.params.userId, req.body.otp);
		res.json(httpUtil.getSuccess('success fully verified', null));
	} catch (error) {
		res.json(httpUtil.getBadRequest(error.toString(), []));
	}
});

route.post('/login', async (req, res) => {
	try {
		await Validation.login(req.body);
		let login = await modalDa.login(req.body);
		if (login[0].contact_num !== req.body.contact_num)
			return res.json(httpUtil.getBadRequest('Incorrect User Name'));
		if (login[0].password !== req.body.password) return res.json(httpUtil.getBadRequest('Incorrect Password'));
		let token = await JwtToken(login[0]);
		let responce = {
			userid: login[0]['user_id'],
			user_name: login[0]['user_name'],
			token: token
		};
		res.json(httpUtil.getSuccess('logged in successfully', [responce]));
	} catch (error) {
		res.json(httpUtil.getException(error.toString(), null));
	}
});

route.post('/forgot_password', async (req, res) => {
	try {
		await Validation.forgotpassword(req.body);
		let otp = generateOtp();
		let userId = await modalDa.forgotpassword(req.body, otp);
		res.json(httpUtil.getSuccess('Otp has been sent', { otp: otp, user_id: userId[0]['user_id'] }));
	} catch (error) {
		res.json(httpUtil.getException(error, null));
	}
});

route.post('/reset_password', async (req, res) => {
	try {
		await Validation.resetpassword(req.body);
		await modalDa.resetpassword(req.body);
		res.json(httpUtil.getSuccess('Password changed Successfully', null));
	} catch (error) {
		res.json(httpUtil.getException(error, null));
	}
});

module.exports = route;
