'use strict';
const sendMail = require('../../utils/mailsender');
const DbCon = require('../../utils/dbConnection');
const QUERY = require('./sql');

const SERVICE = {
	userExistingCheck: async userObj => {
		return await DbCon.query('select * from tbl_users_master where contact_num = ?', userObj['contact_num']);
	},

	saveUser: async userObj => {
		return await DbCon.query(QUERY.saveuser, userObj);
	},
	getUserId: userObj => {
		return DbCon.query(QUERY.getUserId, userObj.contact_num);
	},

	//verify user existence
	Verify: async (userid, otp) => {
		let data = await DbCon.query(QUERY.getuserotp, userid);
		if (data.length === 1 && data[0].verified === 0) {
			if (data[0].otp === otp) {
				await DbCon.query(QUERY.changeStatus, userid);
				return true;
			} else throw 'invalid otp';
		} else throw 'user already confirmed';
	},

	login: async userObj => {
		let data = await DbCon.query('select * from tbl_users_master where contact_num = ?', userObj['contact_num']);
		if (data.length === 1) {
			if (data[0].verified === 1) {
				return data;
			} else {
				throw 'Account Not Verified';
			}
		} else {
			throw 'Invalid User Name';
		}
	},

	forgotpassword: async (data, otp) => {
		let result = await DbCon.query(QUERY.getUserId, data['contact_num']);
		if (result.length === 1) {
			await DbCon.query('update tbl_users_master set verified = 0,otp = ?', otp);
			let a = await DbCon.query('select * from tbl_users_master where contact_num = ?', data['contact_num']);
			return a;
		} else {
			throw 'Number does not exists';
		}
	},
	resetpassword: async data => {
		let result = await DbCon.query(QUERY.getUserId, data['contact_num']);
		if (result.length === 1) {
			if (result[0].password === data.oldpassword) {
				await DbCon.query('update tbl_users_master set password = ? where contact_num = ?', [
					data['newpassword'],
					data['contact_num']
				]);
				return true;
			} else {
				throw 'incorrect old password';
			}
		} else {
			throw 'something went wrong';
		}
	}
};

module.exports = SERVICE;
