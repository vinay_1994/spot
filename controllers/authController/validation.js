'use strict';
const Joi = require('@hapi/joi');

// Method to validate registration values values
exports.registrationValidatioin = async data => {
	var schema = Joi.object().keys({
		mail_id: Joi.string().email({ minDomainSegments: 2 }),
		password: Joi.string()
			.max(100)
			.min(4)
			.required(),
		contact_num: Joi.number()
			.min(10)
			.required(),
		user_name: Joi.string()
			.min(3)
			.required()
	});

	return await Joi.validate(data, schema);
};

exports.login = async data => {
	var schema = Joi.object().keys({
		contact_num: Joi.number()
			.min(10)
			.required(),
		password: Joi.string()
			.max(100)
			.min(4)
			.required()
	});

	return await Joi.validate(data, schema);
};

exports.forgotpassword = async data => {
	var schema = Joi.object().keys({
		contact_num: Joi.number()
			.min(10)
			.required()
	});
	return await Joi.validate(data, schema);
};

exports.resetpassword = async data => {
	var schema = Joi.object().keys({
		contact_num: Joi.number()
			.min(10)
			.required(),
		oldpassword: Joi.string()
			.max(100)
			.min(4)
			.required(),
		newpassword: Joi.string()
			.max(100)
			.min(4)
			.required()
	});

	return await Joi.validate(data, schema);
};
