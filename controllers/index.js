'use strict';
const Router = require('express').Router();

const Api = require('./../constants/api');
const authController = require('./authController');
const dashBoardController = require('./DashboardController');
const AuthCheck = require('../common/security/loginCheck');
const Streamer = require('./../common/streamer').Streamer;
const ProfileController = require('./profileController');

//authentiacation handler
Router.use(Api.AUTH, authController);

//handler for streaming
Router.get(`${Api.DASHBOARD}/:songUrl/:userId/:key`, Streamer);

//dashboard handler
Router.use(Api.DASHBOARD, AuthCheck, dashBoardController);

//profile handler
Router.use(Api.PROFILE, AuthCheck, ProfileController);

module.exports = Router;
