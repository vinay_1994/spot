'use strict';
const DbCon = require('../../utils/dbConnection');

const SERVICE = {
	getProfileDetails: async req => {
		return await DbCon.query('select * from tbl_users_master where user_id = ?', req.user['user_id']);
	},
	updateProfilePic: async (req, picUrl) => {
		return await DbCon.query('update tbl_users_master set profile_pic = ? where user_id = ?', [
			picUrl,
			req.user['user_id']
		]);
	}
};

module.exports = SERVICE;
