'use strict';
const route = require('express').Router();
const httpUtil = require('../../utils/HttpUtil');
const DbService = require('./DbService');
const S3Upload = require('../../utils/s3filesManager/fileupload');

route.get('/', async (req, res) => {
	try {
		let data = await DbService.getProfileDetails(req);
		delete data[0].password;
		delete data[0].otp;
		delete data[0].verified;
		data[0]['profile_pic'] = data[0]['profile_pic'].toString();
		res.json(httpUtil.getSuccess('Success', [data[0]]));
	} catch (error) {
		res.json(httpUtil.getException('Something Went Wrong', error));
	}
});

route.post('/pic_upload', async (req, res) => {
	try {
		S3Upload.uploadToS3(req, res)
			.then(async resolved => {
				await DbService.updateProfilePic(req, resolved.file.location);
				res.json(httpUtil.getSuccess('File Uploaded Successfully', [resolved.file]));
			})
			.catch(rej => {
				console.log(rej);
				res.json(httpUtil.getException('Error in uploading the file', rej.toString()));
			});
	} catch (error) {
		res.json(httpUtil.getException('Something Went Wrong', error));
	}
});

module.exports = route;
