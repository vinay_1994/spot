'use strict';
const otpGenerator = require('otp-generator')
const option = {
    digits:true,
    alphabets:false,
    upperCase:false,
    specialChars:false
}

module.exports.generateOtp = ()=>otpGenerator.generate(4,option)