const AWS = require('aws-sdk');
const SNS = require('./../config/production')

module.exports = (obj) => {

    AWS.config.update({ accessKeyId: SNS.SNS.ACCESS_KEY, secretAccessKey: SNS.SNS.SECREATE_KEY })
    const sns = new AWS.SNS({ region: 'us-east-1' });
    
    let OTP = obj._OTP;
    let phone_Number =obj.PHONE_NUMBER;

    const params = {
        Message: OTP + " "+" Is Your OTP for Registration fro food service " ,
        MessageStructure: 'string',
        PhoneNumber: '+91'+phone_Number
    };
    return new Promise((res,rej) => {
        sns.publish(params, (err, data) => {
           if(!err){
                res(data)
           }else{
               rej(err)
           }
         }); 
    })
   
}

