'use strict';

/**
 * @file HttpUtil is util function, It will provide the support functions to create respones objects.
 * @copyright www.neviton.com
 */

/**
 * If request is sucess
 *
 * @param {any} payload It is any value to send as response.
 * @param {string} errormessage It is string value to send as respone message
 * @return {object} {status=200rmessage='OK', payload=data }
 */
exports.getSuccess = (message,data) => {
  return {
    status: 200,
    message : message,
    payload : data
  };
};

/**
 * If resource is created.
 *
 * @param {any} payload It is any value to send as response.
 * @param {string} errormessage It is string value to send as respone message
 * @return {object} {status=201rmessage='Created', payload=data }
 */
exports.getCreated = (message = 'Created',data) => {
  return {
    status: 201,
    message:message,
    payload:data
  };
};

/**
 * If any invalid request or request data.
 *
 * @param {array} message,is an array of message,e and message,sage.
 * @return {object} {status=400errormessage='Bad Request', payload=null }
 */
exports.getBadRequest = (message,data) => {
  return {
    status: 400,
    message,
    payload: data
  };
};

/**
 * If any server side Exception.
 *
 * @param {array} message,is an array of message,e and message,sage.
 * @return {object} {status=500errormessage='Internal Server Error', payload=null }
 */
exports.getException = (message,data) => {
  return {
    status: 500,
    message: message,
    payload: data
  };
};

/**
 * If any Unauthorized request.
 *
 * @param {array} message,is an array of message,e and message,sage.
 * @return {object} {status=401errormessage='Unauthorized', payload=null }
 */
// UNAUTHORIZED(401, "Unauthorized"),
exports.getUnauthorized = (message='Unauthorized',) => {
  return {
    status: 401,
    message: message,
    payload: null
  };
};

/**
 * If Access denined.
 *
 * @param {array} message,is an array of message,e and message,sage.
 * @return {object} {status=403errormessage='Forbidden', payload=null }
 */
exports.getAccessDenined = (message='Forbidden',data) => {
  return {
    status: 403,
    message: message,
    payload: data
  };
};

/**
 * If Requested data or record is not found.
 *
 * @param {array} message,is an array of message,e and message,sage.
 * @return {object} {status=404errormessage='Not Found', payload=null }
 */




