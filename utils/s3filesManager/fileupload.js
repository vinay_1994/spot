'use strict';
const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const AWS = require('../../config/production').AWS;

const s3 = new aws.S3(AWS);

const upload = multer({
	storage: multerS3({
		s3: s3,
		bucket: function(req, file, cb) {
			cb(null, 'music-app-india' + '/' + req.user['user_id']);
		},
		metadata: function(req, file, cb) {
			cb(null, {
				fieldName: file.fieldname
			});
		},
		key: function(req, file, cb) {
			cb(null, Date.now().toString() + '.' + file.originalname.split('.')[1]);
		}
	})
});

const singleUpload = upload.single('file');

function uploadToS3(req, res) {
	return new Promise((resolve, reject) => {
		return singleUpload(req, res, err => {
			if (err) return reject(err);
			return resolve(res.req);
		});
	});
}

module.exports = { uploadToS3, s3 };
