'use strict'
const JWT = require('jsonwebtoken');

const key = "dont_ask_anything_go";

module.exports = async userdetails =>{
    try {
        let payload = {
            user_id:userdetails['user_id'],
            user_name:userdetails['user_name']
        }
        return await JWT.sign(payload,key);
    } catch (error) {
     throw new Error(error)   
    } 
}