const Jwt = require('jsonwebtoken');
const DbCon = require('./../../utils/dbConnection');
const httpUtil = require('../../utils/HttpUtil');

module.exports = (req, res, next) => {
	try {
		let token = req.headers['authorization'].split(' ')[1];
		if (!token) return res.json(httpUtil.getUnauthorized('Login Required', null));
		Jwt.verify(token, 'dont_ask_anything_go', async (err, data) => {
			if (!err) {
				delete data.iat;
				let contact_num = await DbCon.query(
					'select contact_num from tbl_users_master where user_id = ?',
					data['user_id']
				);
				req.user = data;
				req.contact_num = contact_num[0]['contact_num'];
				return next();
			}
			return res.json(httpUtil.getUnauthorized('Login Required', null));
		});
	} catch (error) {
		res.json(httpUtil.getUnauthorized('Login Required', null));
	}
};
