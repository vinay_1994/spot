const { HttpUtil } = require('../../utils');
const { messages } = require('../../constants');

const { COMMON } = messages;

exports.preAuthorize = (allowedGrants = []) => {
  return (req, res, next) => {
    const { locals = {} } = res;
    if (!locals || !locals.grants || !locals.grants.length) {
      res.json(HttpUtil.getAccessDenined(COMMON.FORBIDDEN));
    } else {
      const { grants = [] } = locals;
      const found = allowedGrants.some(o => grants.indexOf(o) !== -1);
      console.log(found);
      if (found) {
        next();
      } else {
        res.json(HttpUtil.getAccessDenined(COMMON.FORBIDDEN));
      }
    }
  };
};
