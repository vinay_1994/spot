const s3Upload = require('../utils/s3filesManager/fileupload');
const httpUtil = require('./../utils/HttpUtil');

exports.Streamer = (req, res) => {
	try {
		var s3Stream = s3Upload.s3
			.getObject({
				Bucket: 'music-app-india',
				Key: `${req.params.userId}/${req.params.key}`
			})
			.createReadStream();

		s3Stream.on('error', function(err) {
			res.json(httpUtil.getSuccess('Error Happend', err));
		});
		if (req.params.key.split('.')[1] === 'mp4') {
			res.writeHead(200, { 'Content-Type': 'video/mp4' });
		} else {
			res.writeHead(200, { 'Content-Type': 'audio/mpeg' });
		}
		s3Stream
			.pipe(res)
			.on('error', function(err) {
				res.json(httpUtil.getSuccess('Error Happend', err));
			})
			.on('close', function() {
				res.json(httpUtil.getSuccess('Play back completed', []));
			});
	} catch (error) {
		res.json(httpUtil.getException('Something Went Wrong', error));
	}
};
